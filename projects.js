var projects = {
	"list": [
		{
			"type": "program",
			"status": [
				"closed"
			],
			"title": "Girls IN2 STEM",
			"shortDescription": "Peer to peer teen mentoring program",
			"longDescription": "Peer mentoring program which trains high school girls grades 9-12 to mentor middle school girls grades 6 - 8 during monthly STEM related activities.",
			"grandChallengeCategory": "Gender Equality, Quality Educ.",
			"members": [
				"wiSTEM",
				"Girls Who Code",
				"Million Women Mentors"
			],
			"ledBy": [
				{
					"email": "ilee1@imsa.edu",
					"name": "Isabel Lee"
				}
			],
			"projectDrivers": [
				{
					"email": "bhart@imsa.edu",
					"name": "Betty Hart"
				}
			]
		},
		{
			"type": "event",
			"status": [
				"open"
			],
			"title": "Teen STEM Cafe",
			"shortDescription": "STEM events for teens 9-12 grade",
			"longDescription": "Teen STEM Cafe increases teens' awareness and understanding of how science, technology, engineering and mathematics (STEM) affect their lives now and will continue to do so in the future.  Teen STEM Cafe events are informal opportunities for teens to learn about STEM topics and connect with and have fun with other teens interested in STEM. ",
			"grandChallengeCategory": "Quality Ed, Partnership Goals",
			"members": [
				"Teen STEM Board"
			],
			"ledBy": [
				{
					"email": "NIU",
					"name": "Judith Dymond"
				}
			],
			"projectDrivers": [
				{
					"email": "bhart@imsa.edu",
					"name": "Betty Hart"
				}
			]
		},
		{
			"type": "event",
			"status": [
				"closed"
			],
			"title": "Green Apple Day of Service",
			"shortDescription": "Green Apple is a global movement to encourage students to serve eco-friendly initiatives in their community.",
			"longDescription": "Green Apple Day of Service gives parents, teachers, students, companies and local organizations the opportunity to transform their community into healthy, safe and productive learning environments through local service events.",
			"grandChallengeCategory": "Energy, Community, Climate",
			"members": [
				"Club Terra",
				" ComEd"
			],
			"ledBy": [
				{
					"email": "bhart@imsa.edu",
					"name": "Betty Hart"
				}
			],
			"projectDrivers": [
				{
					"email": "bhart@imsa.edu",
					"name": "Betty Hart"
				}
			]
		},
		{
			"type": "program",
			"status": [
				"closed"
			],
			"title": "Makerspace Certification Program",
			"shortDescription": "Exploring the creation of an educational certification program for makerspace managers.",
			"longDescription": "Working with partners Waubonsee Community College and Met-l-flow, IMSA is exploring the possibility of creating a stacked certification program for makerspace managers.",
			"grandChallengeCategory": "education",
			"members": [
				"Carl Dekker - Met-l-Flow",
				"Dr. Sanza Kazadi - skazadi@imsa.edu",
				"Dr. Carl Heine - heine@imsa.edu"
			],
			"ledBy": [
				{
					"name": "Britta McKenna - bmckenna@imsa.edu"
				},
				{
					"name": "Dr. Norman \"Storm\" Robinson - nrobinson@imsa.edu"
				},
				{
					"name": "Dr. Renee Tonioni - rtonioni@waubonsee.edu"
				},
				{
					"name": "Erik Swanson - eswanson@imsa.edu"
				}
			],
			"projectDrivers": [
				{
					"name": "Britta McKenna - bmckenna@imsa.edu"
				}
			]
		},
		{
			"type": "project",
			"status": [
				"closed"
			],
			"title": "IN2 LInkubator",
			"shortDescription": "IN2 hosts shared co- working spaces for area startups (non-profit or tech-enabled) through an application process.",
			"longDescription": "2017-18 LINkubator cohort runs from late August, 2017 through the end of May, 2018. IN2 amenities available to LINkubator cohort members include:\n- Shared workspace (Monday - Friday, times vary with evening hours M-TH) when IMSA is open - located on the IMSA campus at 1500 Sullivan Rd., Aurora, IL 60506\n-Wi-fi\n-Shared office center (copying, general office supplies) l Access to Cafe (refrigerator, microwave, free coffee) l Access to mentors and free programs offered by IN2 partners\n-Access to free IN2 student interns\n-Use of makerspace equipment (3D printers, small CNC, prototyping tools) for an additional fee\nOnline application available at: goo.gl/5PZx7e",
			"grandChallengeCategory": "Industry Innovation, Decent Wo",
			"members": [
				"Terrie Simmons - Women's Business Development Center",
				"Judy Dawnson - Invest Aurora",
				"Betty Hart - bhart@imsa.edu"
			],
			"ledBy": [
				{
					"name": "Sue Fricano - sfricano@imsa.edu"
				}
			],
			"projectDrivers": [
				{
					"name": "Britta McKenna - bmckenna@imsa.edu"
				}
			]
		},
		{
			"type": "project",
			"status": [
				"closed"
			],
			"title": "INspire",
			"shortDescription": "INspire is dedicated to making elementary and middle schoolers in our community feel comfortable expressing, supporting, and selling their ideas through our module series. ",
			"longDescription": "INspire is an IN2 project team dedicated to spreading a love for public speaking among the elementary and middle school students (4th-8th grade) in our community. Our activity-based curriculum consists of modules in impromptu, debate, and pitching where students learn to express, support, and sell their ideas. With our focus group activities, students get personalized feedback from professionals and trained high schoolers on their speaking skills and suggestions on how to improve them. In addition to holding more INspire modules at IN2 and expanding our curriculum, we are currently working with nonprofit organizations to incorporate INspire into their program offerings. ",
			"grandChallengeCategory": "education",
			"members": [
				"Organization: James R. Jordan Foundation\nMembers: Sonya Gupta",
				"Madi Mazzorana",
				"Meghan Hendrix",
				"Urvi Awasthi",
				"Grace Sleyko"
			],
			"ledBy": [
				{
					"email": "emathew@imsa.edu",
					"name": "Esther Mathew"
				},
				{
					"email": "chultquist@imsa.edu",
					"name": "Charlie Hultquist"
				},
				{
					"email": "rjayewickreme@imsa.edu",
					"name": "Radeesha Jayewickreme"
				},
				{
					"email": "etang@imsa.edu",
					"name": "Elizabeth Tang"
				},
				{
					"email": "lliu@imsa.edu",
					"name": "Lucy Liu"
				},
				{
					"email": "cstrauch@imsa.edu",
					"name": "Clayton Strauch"
				},
				{
					"email": "rxun@imsa.edu",
					"name": "Rebecca Xun"
				}
			],
			"projectDrivers": [
				{
					"email": "rebreisch@alum.mit.edu",
					"name": "Roger Breisch"
				},
				{
					"email": "maureen@globalwlf.com",
					"name": "Maureen O'Brien"
				}
			]
		},
		{
			"type": "project",
			"status": [
				"closed"
			],
			"title": "Dropdot",
			"shortDescription": "Using dots to teach preschoolers letter and number recognition.",
			"longDescription": "Dropdot is a smartphone app for early education that uses dot to dot drawings to teach and reinforce number and letter recognition as well as counting and spelling",
			"grandChallengeCategory": "Education",
			"members": [
				"Robert Chang",
				"Namrata Pandya"
			],
			"ledBy": [
				{
					"email": "fchen@imsa.edu",
					"name": "Felicia Chen"
				},
				{
					"email": "vsuriyanarayanan@imsa.edu",
					"name": "Vaidehi Suriyanarayanan"
				},
				{
					"email": "gyang@imsa.edu",
					"name": "Grace Yang"
				},
				{
					"email": "aorlov@imsa.edu",
					"name": "Alex Orlov"
				},
				{
					"email": "sdragan@imsa.edu",
					"name": "Sandra Dragan"
				},
				{
					"email": "mramanan@imsa.edu",
					"name": "Megha Ramanan"
				}
			],
			"projectDrivers": [
				{
					"email": "heine@imsa.edu",
					"name": "Carl Heine"
				}
			]
		},
		{
			"type": "event",
			"status": [
				"open"
			],
			"title": "IMSA Day of Cyber",
			"shortDescription": "Interactive cyber career focused event is geared towards students in 6th - 12th grade.  ",
			"longDescription": "IMSA Day of Cyber event is geared towards students in 6th - 12th grade.  \nIMSA's NEW IN2 Center has partnered with the Midwest Cyber Center of Excellence on a program, sponsored by the National Security Agency, and powered by Life Journey, called the NSA Day of Cyber AN \"INTERACTIVE, SELF-GUIDED, AND FULLY-AUTOMATED CYBERSECURITY CAREER EXPERIENCE\". Our goal is to introduce students to the skills and education pathways needed to achieve those cyber career futures in the lucrative cyber security market.   We need mentors from all industries to share their experiences and passions for their cyber security career fields.",
			"grandChallengeCategory": "4- Quality Education, 8-Decent Work and Economic Growth, 9-Industry Innovation and Infrastructure & 11-Sustainable Cities and Communities",
			"members": [
				"MIke Xu",
				"William Tong",
				"Spoorthi Jakka",
				"Jessica Lee"
			],
			"ledBy": [
				{
					"name": "Betty Hart"
				},
				{
					"name": "bhart@imsa.edu"
				},
				{
					"name": "Norman Robinson"
				},
				{
					"name": "nrobinson@imsa.edu"
				},
				{
					"name": "Richard Bugsby"
				},
				{
					"name": "rbugsby@imsa.edu"
				},
				{
					"name": "Namrata Pandya"
				},
				{
					"name": "npandya@imsa.edu"
				},
				{
					"name": "IMSA Cyber Student board"
				}
			],
			"projectDrivers": [
				{
					"name": "Betty Hart"
				}
			]
		},
		{
			"type": "program",
			"status": [
				"closed"
			],
			"title": "eleMENT",
			"shortDescription": "Introducing students to Lean Startup methods",
			"longDescription": "Lean Startup methods help companies build, test and refine products and services with minimum investment of time and money. IMSA sophomores are introduced to this type of thinking as a first step in entrepreneurship.",
			"grandChallengeCategory": "Each company prototype specifies its own Challenge target(s)",
			"members": [
				"Startups at IN2 and 1871"
			],
			"ledBy": [
				{
					"name": "Vainius Normantas"
				},
				{
					"name": "Ashritha Karuturi"
				},
				{
					"name": "Evan Sun"
				},
				{
					"name": "Charlie Hulquist"
				},
				{
					"name": "Rebecca Lisk"
				}
			],
			"projectDrivers": [
				{
					"email": "heine@imsa.edu",
					"name": "Carl Heine"
				}
			]
		},
		{
			"type": "program",
			"status": [
				"closed"
			],
			"title": "IMSA TALENT",
			"shortDescription": "Entrepreneurship education for grades 6 to 12",
			"longDescription": "TALENT helps students find problems, create and test solutions, form teams, develop business models and, ultimately, operational business entities",
			"grandChallengeCategory": "This varies depending on the problem selected for development",
			"members": [
				"Businesses",
				"startups",
				"investors and other mentors in the Chicagoland area"
			],
			"ledBy": [
				{
					"email": "heine@imsa.edu",
					"name": "Carl Heine"
				},
				{
					"email": "jgerry@imsa.edu",
					"name": "Jim Gerry"
				}
			],
			"projectDrivers": [
				{
					"name": "Carl Heine"
				}
			]
		},
		{
			"type": "project",
			"status": [
				"closed"
			],
			"title": "Aviation Club",
			"shortDescription": "Aviation club is a collaborative environment for individuals of all experience levels to learn from each other and get hands on experience with aviation technology like quadcopters and planes.",
			"longDescription": "Aviation club a group for individuals interested in aviation to work with the technology. The club is open to any level of experience from those who have never seen a quadcopter before to those who have built three of them in their backyard. Through collaboration with IN2's makerspace, Aviation club has access to its own full sized and miniature quadcopters as well as the materials and technology for the group’s prototypes and designs.",
			"grandChallengeCategory": "Future Technology & Education",
			"members": [
				"Experimental Aircraft Association (EAA)"
			],
			"ledBy": [
				{
					"name": "Kathryn Downey"
				}
			],
			"projectDrivers": [
				{
					"name": "N/A"
				}
			]
		}
	]
};
