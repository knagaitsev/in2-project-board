/*
var projects = {
    list: [
        {
            title: "Doorbell Attachment",
            type: "project",
            status: ["closed", "cool"],
            shortDescription: "Creating an attachment that covers a doorbell and recognizes when it's rang to play a recorded message while doing other stuff as well while doing things and stuff.",
            longDescription: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
            ledBy: [
                {
                    name: "Lia White",
                    email: "lwhite@imsa.edu"
                },
                {
                    name: "Lin White",
                    email: "lwhite@imsa.edu"
                }
            ],
            grandChallengeCategory: "Thing",
            projectDrivers : [
                {
                    name: "Micah Fogel",
                    email: "fogel@imsa.edu"
                },
                {
                    name: "Erik Swanson",
                    email: "eswanson@imsa.edu"
                }
            ],
            members: [
                "Renzo Leclesma",
                "Grace Mitchell",
                "Urvi Awasthi",
                "Erma Porter",
                "Jacquelyn Butler"
            ]
        }
    ]
};
*/

var currentView = "project";
/*
var markers = null;
$(document).ready(function () {
    $.get("http://bitnami-openproject-04be.cloudapp.net/projects.atom?key=fa833682b571fb128fadf95e64d94016efe8ca4b", {}, function (xml){
      $('marker',xml).each(function(i){
         markers = $(this);
      });
    });
});

//read the RSS feed
//http://bitnami-openproject-04be.cloudapp.net/projects.atom?key=fa833682b571fb128fadf95e64d94016efe8ca4b
$.jQRSS('http://bitnami-openproject-04be.cloudapp.net/projects.atom?key=83e99c0a7d4dd09be357fe01cace3dfbd6587306',
 { count: 8 },
    function(feed) {
        console.log(feed);
        //iterate through entries, create divs to contain information
    	for (var i in feed.entries) {
    		var title = feed.entries[i]['title'];
            //console.log(title);
    		var newTitle = title.split(" - Project: ");
    		$('#scrolling').append('<div class="div' + i + '"></div>');
    		$('.div' + i).append('<h1> ' + newTitle[0] + '</h1>');
    		$('.div' + i).append('<p> ' + feed.entries[i]['content'] + '</p>');
    	}
    	$(document).ready(function() {
            setStatus("status");
    		$('#scrolling p:has(strong)').fadeOut(0);
    		clickExpand();
            autoScroll();
		});
    }
);
*/
$(document).ready(function() {
    initProjectElements();
    autoScroll();

    //fade out blue popup when close button is clicked
	$('#popup span').click(function() {
		$('#popup').toggle(450);
	});

    //active button is clicked, change view to active
    $('#project').click(function() {
        changeView('project');
    });

    //dormant button clicked
    $('#program').click(function() {
        changeView('program');
    });

    $("#event").click(function() {
        changeView('event');
    });

});
/*
//give a project a specific class name based on an attribute from the feed
function setStatus(attribute) {
    for (var i in $('#scrolling div').toArray()) {
        var details = $('.div' + i + ' strong').toArray();
        for (var a in details) {
            var phrase = attribute + ": ";
            if (details[a].textContent.includes(phrase)) {
                var className = details[a].textContent.replace(phrase, "");
                $('.div' + i).addClass(className);
            }
        }
    }
}*/

function autoScroll() {
    window.setInterval(scrollAnimation, 10000);
}

function scrollAnimation() {
    var time = 500;
    $('#scrolling div.' + currentView + ':first').fadeOut(time, function() {
        var originalMargin = $('#scrolling').css("margin-top");
        $('#scrolling div.' + currentView + ':first').insertAfter('#scrolling div:last');
        var topMargin = $('#scrolling div.' + currentView + ':last').outerHeight(true) + 20;
        $('#scrolling').css("margin-top", topMargin);
        $('#scrolling').animate({marginTop: originalMargin}, time);
        $('#scrolling div.' + currentView + ':last').fadeIn(time);
    });
}
/*
//this function expands a project to the main area, when clicked
function clickExpand() {
    for (var i in $('#scrolling div').toArray()) {
        $('.div' + i).click(function(event) {
            //$('#popup').fadeOut(400);
            var box = '#info #content';
            $(box).empty();
            cn = this.className.split(" ")[0];
            var projId = $('.' + cn +' h1').text();//.split(' ').join('&nbsp');
            $('.' + cn + ' h1').clone().appendTo(box);
            $('.' + cn + ' p').clone().appendTo(box);

            var text = getData(cn);
            insertData(text, projId);
            $('.' + cn + ' ul').clone().appendTo(box);
            $('.' + cn + ' img').clone().appendTo(box);

            if($(box + ' img').length == 0) {
                $('#in2-logo').fadeIn(0);
            }
            else {
                $('#in2-logo').fadeOut(0);
            }
            $('#info p:has(strong)').fadeOut(0);
            $('#btns').fadeIn(100);
        });
    }
}

function getData(className) {
    var details = $('.' + className + ' strong').toArray();
    var data = {};
    for (var i in details) {
        var rawData = details[i].textContent.split(":");
        var key = rawData[0].toLowerCase();
        var val = rawData[1].replace(/\s+/g, '');
        data[key] = val;
    }

    return data;
}

function insertData(data, id) {
    var driverNames = data["champion name"].split(",");
    var driverEmails = data["champion email"].split(",");

    var text = '<h4><i class="fa fa-check-square-o" aria-hidden="true"></i>&nbsp&nbspSubmitted By</h4>' +
        '<p>' + data["submit name"] + '&nbsp&nbsp<span id="blueBox">' + data["submit date"] +' </span>' + '</p>' +
        '<h4><i class="fa fa-globe" aria-hidden="true"></i>&nbsp&nbspGlobal Grand Challenge Category</h4>' +
        '<p>' + data["category"] + '</p>' +
        '<h4><i class="fa fa-trophy" aria-hidden="true"></i>&nbsp&nbspProject Driver (Champion)</h4>';

    for (var i in driverNames) {
        text += '<p>' + driverNames[i] +
            '&nbsp<span id="blueBox" class="email-box"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp' +
            driverEmails[i] +' </span></p>';
    }
    text += '<h4><i class="fa fa-users" aria-hidden="true"></i>&nbsp&nbspCollaborators/Interested Members</h4>';

    $('#info #content').append(text);

    $('#apply').attr("href", "https://imsa-in2.typeform.com/to/PBMfIx?project=" +
        id + "&driver_email=" + data["champion email"]);
}
*/
function changeView(view) {
    var time = 500;
    if (currentView != view) {
        $("#scrolling").children().css("display", "none");
        $("." + view).fadeIn(time);
        currentView = view;
    }
    /*
    if (view == "active" && currentView == "dormant") {
        $('.dormant').css("display", "none");
        $('.active').fadeIn(time);
        currentView = "active";
    }
    else if (view == "dormant" && currentView == "active") {
        $('.active').css("display", "none");
        $('.dormant').fadeIn(time);
        currentView = "dormant";
    }
    */
}

function addProjectElements() {
    projects.list.forEach(function(project, index) {
        var elem = $("<div></div>");
        elem.attr("class", project.type);
        elem.attr("project", index);
        $("<h1>" + project.title + "</h1>").appendTo(elem);
        $("<p>" + project.shortDescription + "</p>").appendTo(elem);
        elem.appendTo("#scrolling");
    });
}

function initProjectElements() {
    addProjectElements();
    $("#content").hide();
    $("#scrolling div").click(function() {
        var self = this;
        if ($("#content").css("display") == "none") {
            setMainContent($(self).attr("project"));
            $("#content").toggle(500);
        }
        else {
            $("#content").toggle(500, function() {
                setMainContent($(self).attr("project"));
                $("#content").toggle(500);
            });
        }
        $("#btns").fadeIn(100);
    });
}

function setMainContent(projNum) {
    projNum = parseInt(projNum);
    //console.log(projNum);
    var proj = projects.list[projNum];

    var applyURL = "https://imsa-in2.typeform.com/to/PBMfIx?project=" + proj.title +
        "&driver_email=";

    $("#proj-title").text(proj.title);
    $("#proj-status").empty();
    var statusTxt = "";
    proj.status.forEach(function(status, index) {
        statusTxt += status.toUpperCase();
        if (index != proj.status.length - 1) {
            statusTxt += ", ";
        }
    });
    $("#proj-status").text(statusTxt);

    $("#proj-long-desc").text(proj.longDescription);

    $("#proj-led-by").empty();
    $(".led-by-title").show();
    if (proj.ledBy.length === 0) {
        $(".led-by-title").hide();
    }
    proj.ledBy.forEach(function(leader, index) {
        var emailStr = "";
        if (leader.email !== null && leader.email !== undefined && leader.email !== "") {
            var icon = "";
            if (leader.email.includes("@")) {
                icon += '<i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp';
            }
            else {

            }
            emailStr = '&nbsp<span id="blueBox" class="email-box">' +
              icon +
              '<span id="proj-driver-email">' + leader.email + '</span>' +
              '</span>';
        }
        $('<p><span id="proj-driver">' + leader.name + '</span>' + emailStr +
        '</p>').appendTo("#proj-led-by");
    });

    $("#project-gcc").text(proj.grandChallengeCategory);

    $("#proj-driver-container").empty();
    $(".driver-title").show();
    if (proj.projectDrivers.length === 0) {
        $(".driver-title").hide();
    }
    proj.projectDrivers.forEach(function(driver, index) {
        var emailStr = "";
        if (driver.email !== null && driver.email !== undefined && driver.email !== "") {
            emailStr = '&nbsp<span id="blueBox" class="email-box">' +
              '<i class="fa fa-envelope-o" aria-hidden="true"></i> ' +
              '<span id="proj-driver-email">' + driver.email + '</span>' +
              '</span>';
        } 
        $('<p><span id="proj-driver">' + driver.name + '</span>' + emailStr +
        '</p>').appendTo("#proj-driver-container");
          applyURL += driver.email + ",";
    });
    applyURL = applyURL.substring(0, applyURL.length-1);
    $("#btns #apply").attr("href", applyURL);

    if (proj.status.indexOf("closed") != -1) {
        $("#apply-title").text("Contribute");
    }
    else if (proj.status.indexOf("open") != -1) {
        $("#apply-title").text("Join Now");
    }

    $("#proj-member-container").empty();
    $(".member-title").show();
    if (proj.members.length === 0) {
        $(".member-title").hide();
    }
    proj.members.forEach(function(member, index) {
        $("<li>" + member + "</li>").appendTo("#proj-member-container");
    });
}
